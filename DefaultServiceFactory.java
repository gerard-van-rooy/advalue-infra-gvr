package net.persgroep.adhub.infrastructure;

import static java.lang.Integer.parseInt;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

import javax.sql.DataSource;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.lambda.AWSLambdaAsyncClientBuilder;
import com.amazonaws.services.lambda.invoke.LambdaInvokerFactory;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;

import net.persgroep.adhub.NotificationService;
import net.persgroep.adhub.ServiceFactory;
import net.persgroep.adhub.adpointix.MaterialRepository;
import net.persgroep.adhub.adpointix.MaterialService;
import net.persgroep.adhub.adsml.PlanpointService;
import net.persgroep.adhub.adsml.PubbleService;
import net.persgroep.adhub.ast.AstService;
import net.persgroep.adhub.core.MaterialDestination;
import net.persgroep.adhub.event.OrderEventService;
import net.persgroep.adhub.exception.AdHubException;
import net.persgroep.adhub.infrastructure.aws.function.PlacedAdsFunction;
import net.persgroep.adhub.infrastructure.mock.DataSourceMock;
import net.persgroep.adhub.infrastructure.mock.PlacedAdServiceMock;
import net.persgroep.adhub.infrastructure.repositories.MaterialRepositoryDynamoDB;
import net.persgroep.adhub.lineup.LineupService;
import net.persgroep.adhub.lineup.LineupServiceConfig;
import net.persgroep.adhub.lineup.MediaService;
import net.persgroep.adhub.net.ftp.FtpConfig;
import net.persgroep.adhub.net.ftp.FtpService;
import net.persgroep.adhub.net.ftp.FtpServiceFactory;
import net.persgroep.adhub.pagesize.PageSizeService;
import net.persgroep.adhub.pagination.planner.PlannerPageService;
import net.persgroep.adhub.pagination.planpoint.PlanpointPageService;
import net.persgroep.adhub.planner.PlannerService;
import net.persgroep.adhub.reports.AdHubReportService;
import net.persgroep.adhub.reports.ReportService;
import net.persgroep.adhub.vouching.CountryService;
import net.persgroep.adhub.vouching.PlacedAdService;
import net.persgroep.adhub.vouching.VouchingService;
import net.persgroep.adhub.vouching.digital.EbiquityVouchingService;
import net.persgroep.adhub.vouching.domain.model.PublicationSystem;
import net.persgroep.adhub.vouching.planner.PlannerPlacedAdService;
import net.persgroep.adhub.vouching.printed.DrukkerijVouchingProcessor;
import net.persgroep.adhub.vouching.printed.SpreadItVouchingProcessor;
import net.persgroep.adhub.vouching.pubble.PubblePlacedAdService;
import net.persgroep.adhub.xtralocal.PacketNameService;
import net.persgroep.advalue.orderevent.adsml.OrderEventToAdsml;
import oracle.jdbc.pool.OracleDataSource;
import oracle.ucp.jdbc.PoolDataSource;
import oracle.ucp.jdbc.PoolDataSourceFactory;

public class DefaultServiceFactory extends ServiceFactory {

    private final Map<String, Object> services = new HashMap<>();

    public DefaultServiceFactory() {
        super();
    }

    @SuppressWarnings("unchecked")
    @Override
    public <S> S findOrCreateService(String serviceName, Supplier<S> supplier) {
        if (!services.containsKey(serviceName)) {
            services.put(serviceName, supplier.get());
        }

        return (S) services.get(serviceName);
    }

    @Override
    public OrderEventToAdsml newAdpointService() {
        return findOrCreateService("adpoint-service", () -> new OrderEventToAdsml("ADPOINT_IX_SYSTEM_ID"));
    }

    @Override
    public PacketNameService newPacketNameService() {
        return findOrCreateService("packet-name-service", () -> new PacketNameService());
    }

    @Override
    public OrderEventService newEventService() {
        return findOrCreateService("event-service", eventServiceSupplier());
    }

    private Supplier<OrderEventService> eventServiceSupplier() {
        return () -> new OrderEventService(newLineupService(), new PageSizeService());
    }

    @Override
    public FtpService newPubbleFtpService() {
        return newFtpService(MaterialDestination.PUBBLE);
    }

    @Override
    public NotificationService newNotificationService() {
        return findOrCreateService("notification-service",
                () -> new SNSNotificationService(getEnvironmentVariable("NOTIFICATION_TOPIC_ARN", false)));
    }

    @Override
    public FtpService newFtpService(FtpConfig ftpConfig) {
        return newFtpServiceSupplier(ftpConfig).get();
    }

    @Override
    public void close() {
        services.values().stream().filter(this::isCloseable).map(CloseableService.class::cast).forEach(this::close);
        services.clear();
    }

    @Override
    public ReportService newReportService() {
        return new AdHubReportService();
    }

    @Override
    public PlacedAdService newPlacedAdService(PublicationSystem publicationSystem) {
        switch (publicationSystem) {
            case PLANNER:
                String plannerHost = getEnvironmentVariable("PLANNER_HOST");
                if (plannerHost.equals("MOCK")) {
                    return new PlacedAdServiceMock();
                }
                return new PlannerPlacedAdService(plannerHost, newLineupService());
            case PUBBLE:
                String pubbleHost = getEnvironmentVariable("PUBBLE_HOST");
                if (pubbleHost.equals("MOCK")) {
                    return new PlacedAdServiceMock();
                }

                return new PubblePlacedAdService(
                        pubbleHost,
                        getEnvironmentVariable("PUBBLE_API_KEY"),
                        getEnvironmentVariable("PUBBLE_DATABASE"),
                        newLineupService());
            default:
                throw new AdHubException("Unknown publication system %s", publicationSystem);
        }
    }

    @Override
    public VouchingService newVouchingService() {
        return new VouchingService(
                new EbiquityVouchingService(
                        newPlacedAdService(PublicationSystem.PLANNER),
                        newPlacedAdService(PublicationSystem.PUBBLE),
                        new MediaService(newLineupService()), newAstService()),
                new DrukkerijVouchingProcessor(),
                new SpreadItVouchingProcessor());
    }

    private boolean isCloseable(Object o) {
        return CloseableService.class.isAssignableFrom(o.getClass());
    }

    private void close(CloseableService service) {
        try {
            service.close();
        } catch (Exception e) {
            // Ignore
        }
    }

    private static Supplier<FtpService> newFtpServiceSupplier(FtpConfig ftpConfig) {
        return () -> FtpServiceFactory.newService(ftpConfig);
    }

    @Override
    public AstService newAstService() {
        return findOrCreateService("ast-service", astServiceSupplier());
    }

    private static Supplier<AstService> astServiceSupplier() {
        return AstService::new;
    }

    @Override
    public LineupService newLineupService() {
        return findOrCreateService("lineup-service", lineupServiceSupplier());
    }

    private Supplier<LineupService> lineupServiceSupplier() {
        return () -> {
            LineupServiceConfig config = new LineupServiceConfig();
            config.setHost(getEnvironmentVariable("LINEUP_HOST"));
            config.setUser(getEnvironmentVariable("LINEUP_USERNAME"));
            config.setPass(getEnvironmentVariable("LINEUP_PASSWORD"));
            config.setNuid(parseInt(getEnvironmentVariable("LINEUP_NUID")));
            config.setIdOffset(parseInt(getEnvironmentVariable("LINEUP_ID_OFFSET")));

            return new LineupService(config);
        };
    }

    private DataSource newPlannerSource() {
        return findOrCreateService("planner-source", plannerSourceCreator());
    }

    private Supplier<DataSource> plannerSourceCreator() {
        return () -> {
            try {
                String datasourceUrl = getEnvironmentVariable("PLANNER_DATASOURCE_URL");

                if (datasourceUrl == "MOCK") {
                    return new DataSourceMock();
                }
                return getDataSource(datasourceUrl);
            } catch (SQLException e) {
                throw new AdHubException(e);
            }
        };
    }

    private DataSource getDataSource(String datasourceUrl) throws SQLException {
        PoolDataSource result = PoolDataSourceFactory.getPoolDataSource();
        result.setConnectionFactoryClassName(OracleDataSource.class.getName());
        result.setURL(datasourceUrl);
        result.setUser(getEnvironmentVariable("PLANNER_DATASOURCE_USERNAME"));
        result.setPassword(getEnvironmentVariable("PLANNER_DATASOURCE_PASSWORD"));
        result.setMinPoolSize(0);
        return result;
    }

    @Override
    public PlannerService newPlannerService() {
        return findOrCreateService("planner-service", plannerServiceCreator());
    }

    private Supplier<PlannerService> plannerServiceCreator() {
        String adDir = getEnvironmentVariable("PLANNER_AD_DIR");

        return () -> new PlannerService(new AstService(), newPlannerSource(), adDir);
    }

    @Override
    public MaterialService newMaterialService() {
        return findOrCreateService("materialService", materialServiceSupplier());
    }

    private Supplier<MaterialService> materialServiceSupplier() {
        return () -> new MaterialService(newLineupService(), newMaterialRepository(), AmazonS3ClientBuilder.defaultClient(), this);
    }

    @Override
    public MaterialRepository newMaterialRepository() {
        return findOrCreateService("material-repository", MaterialRepositoryDynamoDB::new);
    }

    @Override
    public PlanpointService newPlanpointService() {
        return findOrCreateService("planpoint-service",
                () -> new PlanpointService(newAstService(), "PLANPOINT_SYSTEM_ID"));
    }

    @Override
    public PubbleService newPubbleService() {
        return findOrCreateService("pubble-service", () -> new PubbleService("PUBBLE_SYSTEM_ID"));
    }

    @Override
    public PlannerPageService newPlannerPageService() {
        String headerDir = getEnvironmentVariable("PLANNER_HEADER_DIR");
        String fillerDir = getEnvironmentVariable("PLANNER_FILLER_DIR");

        return new PlannerPageService(newPlannerSource(), headerDir, fillerDir);
    }

    @Override
    public PlanpointPageService newPlanpointPageService() {
        return new PlanpointPageService();
    }

    @Override
    public ReportService getReportService() {
        return new AdHubReportService();
    }

    @Override
    public PlacedAdsFunction newPlacedAdsFunction() {
        return getFunctionInstance(PlacedAdsFunction.class);
    }

    public <T> T getFunctionInstance(Class<T> type) {
        return LambdaInvokerFactory.builder()
                .lambdaClient(AWSLambdaAsyncClientBuilder.standard()
                        .withRegion(Regions.EU_WEST_1)
                        .build())
                .lambdaFunctionNameResolver((method, annotation, config) -> annotation.functionName() + "-" + getEnvironmentVariable("Profile"))
                .build(type);
    }

    @Override
    public CountryService newCountryService() {
        return findOrCreateService("country-service", CountryService::new);
    }

}
