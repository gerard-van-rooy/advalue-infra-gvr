package net.persgroep.adhub.lineup;

public class LineupRestServiceTest {
/*
    private static final Logger log = (Logger) LoggerFactory.getLogger(LineupRestServiceTest.class);

    @Autowired
    private LineupService lineupService;

    @Autowired
    private MockServerRestTemplateCustomizer customizer;

    @Autowired
    private MockRestServiceServer server;

    @BeforeClass
    public static void prepare() {
        System.setProperty(LINEUP_HOST, "test");
        System.setProperty(LINEUP_USER, "test");
        System.setProperty(LINEUP_PASSWORD, "test");
        System.setProperty(LINEUP_NUID, "-1");
    }


    @Test
    public void testGetOrders() throws IOException {
        this.server
                .expect(requestTo("https://test/WebAPI/v1/IntegrationXOrders?EventSubscriber=DPN_AV&MaxRecords=1000000"))
                .andRespond(withSuccess(readAsString(getClass().getResourceAsStream("/order-274.xml")), MediaType.APPLICATION_XML));
        ArrayOfXOrderSearchResultRecordType orders = lineupService.getOrders();

        assertThat(orders.getXOrderSearchResultRecord().size()).isEqualTo(1);
    }

    @Test
    public void testGetCustomer() throws IOException {
        this.server
                .expect(requestTo("https://test/WebAPI/v1/Customer/2"))
                .andRespond(withSuccess(readAsString(getClass().getResourceAsStream("/customer-2.xml")), MediaType.APPLICATION_XML));
        CustomerRecordType customerById = lineupService.getCustomerById("2");

        assertThat(customerById).isNotNull();
    }

    @Test
    public void testGetMedia() throws IOException {
        this.server
                .expect(requestTo("https://test/WebAPI/v1/Media?MediaID=70"))
                .andRespond(withSuccess(readAsString(getClass().getResourceAsStream("/media-70.xml")), MediaType.APPLICATION_XML));
        ArrayOfMediaSearchResultRecordType arrayOfMediaSearchResultRecordType = lineupService.getMediaById("70");

        assertThat(arrayOfMediaSearchResultRecordType).isNotNull();
        assertThat(arrayOfMediaSearchResultRecordType.getMediaSearchResultRecord().size()).isEqualTo(1);
    }

    @Test
    public void testGetAds() throws IOException {
        this.server
                .expect(requestTo("https://test/WebAPI/v1/ads?OrderID=274"))
                .andRespond(withSuccess(readAsString(getClass().getResourceAsStream("/ads-274.xml")), MediaType.APPLICATION_XML));
        ArrayOfAdSearchResultRecordType arrayOfAdSearchResultRecordType = lineupService.getAdsByOrderId("274");

        assertThat(arrayOfAdSearchResultRecordType).isNotNull();
        assertThat(arrayOfAdSearchResultRecordType.getAdSearchResultRecord().size()).isEqualTo(32);
    }

    @Test
    public void testGetUsers() throws IOException {
        this.server
                .expect(requestTo("https://test/WebAPI/v1/Users?UserName=Administrator"))
                .andRespond(withSuccess(readAsString(getClass().getResourceAsStream("/user-administrator.xml")), MediaType.APPLICATION_XML));
        ArrayOfUserSearchResultRecordType arrayOfUserSearchResultRecordType = lineupService.getUsersByName("Administrator");

        assertThat(arrayOfUserSearchResultRecordType).isNotNull();
        assertThat(arrayOfUserSearchResultRecordType.getUserSearchResultRecord().size()).isEqualTo(1);
    }

    private String readAsString(InputStream resourceAsStream) throws IOException {
        ByteArrayOutputStream output = new ByteArrayOutputStream();

        try {

            IOUtils.copy(resourceAsStream, output);

            return new String(output.toByteArray());
        } finally {
            IOUtils.closeQuietly(resourceAsStream);
            IOUtils.closeQuietly(output);
        }
    }*/
}