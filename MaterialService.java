package net.persgroep.adhub.adpointix;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.json.bind.JsonbBuilder;

import net.persgroep.advalue.orderevent.adsml.TypesMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.Headers;
import com.amazonaws.services.s3.model.ObjectMetadata;

import net.persgroep.adhub.ServiceFactory;
import net.persgroep.adhub.core.Material;
import net.persgroep.adhub.core.MaterialDestination;
import net.persgroep.adhub.core.Order;
import net.persgroep.adhub.lineup.LineupService;
import net.persgroep.adhub.task.TaskType;

public class MaterialService {
    private static final Logger LOGGER = LoggerFactory.getLogger(MaterialService.class);

    private static final int STATUS_READY = 1;

    private final LineupService lineupService;
    private final MaterialRepository materialRepository;
    private final String queueBucket;
    private final AmazonS3 s3;

    public MaterialService(final LineupService lineupService, final MaterialRepository materialRepository, final AmazonS3 s3,
            final ServiceFactory serviceFactory) {
        this.lineupService = lineupService;
        this.materialRepository = materialRepository;
        this.s3 = s3;
        this.queueBucket = serviceFactory.getEnvironmentVariable("QUEUE_BUCKET", true);
    }

    public Material materialIsReady(final Order order, final Material material) {
        Material result = updateLineup(material);
        result = findAndUpdateMaterial(result);
        return distributeMaterial(order, material);

    }

    private Material findAndUpdateMaterial(final Material material) {
        LOGGER.info("Starting update for material {}.", material.getId());
        Material result = findMaterialById(material.getId())
                .map(existingMaterial -> updateMaterialData(material, existingMaterial))
                .orElse(material);

        result = materialRepository.save(result);
        LOGGER.debug("Finishing update for material {}.", material.getId());

        return result;
    }

    private static Material updateMaterialData(final Material newMaterial, Material existingMaterial) {
        LOGGER.info("Updating material {}", existingMaterial.getId());

        Optional.ofNullable(newMaterial.getMaterialUrl())
                .map(String::trim)
                .filter(s -> !s.isEmpty())
                .ifPresent(existingMaterial::setMaterialUrl);

        Optional.ofNullable(newMaterial.getPreviewUrl())
                .map(String::trim)
                .filter(s -> !s.isEmpty())
                .ifPresent(existingMaterial::setPreviewUrl);

        return existingMaterial;
    }

    private Collection<Material> distributeMaterials(final Order order, final Collection<Material> materials) {
        return materials.stream().map(material -> distributeMaterial(order, material)).collect(Collectors.toList());
    }

    private Material distributeMaterial(final Order order, final Material material) {
        final Integer id = material.getId();
        LOGGER.debug("Material: {} busy", id);

        final String previewUrl = material.getPreviewUrl();
        final String materialUrl = material.getMaterialUrl();
        if (previewUrl != null || materialUrl != null) {
            LOGGER.debug("Material: {} actual updating something to {}", id,
                    material.getDestinations().stream()
                            .map(dest -> dest.name())
                            .collect(Collectors.joining(", ")));
            for (MaterialDestination destination : material.getDestinations()) {
                distributeMaterialToDestination(order, material, id, destination);
            }
        } else {
            LOGGER.debug("Material: {} nothing to do", id);
        }

        return material;
    }

    private void distributeMaterialToDestination(final Order order, final Material material, final Integer id, final MaterialDestination destination) {
        LOGGER.debug("Material: {} busy with {}", id, destination);
        final String previewUrl = material.getPreviewUrl();
        final String materialUrl = material.getMaterialUrl();

        try {
            if (destination.equals(MaterialDestination.PUBBLE)) {
                if (previewUrl != null) {
                    final String jpgName = TypesMapper.mapIdentifierWithOffset(id) + ".jpg";
                    LOGGER.info("PUBBLE->material jpg name: '{}'", jpgName);
                    handleQueue(destination, jpgName, previewUrl);
                }

                if (materialUrl != null) {
                    final String pdfName = TypesMapper.mapIdentifierWithOffset(id) + ".pdf";
                    LOGGER.info("PUBBLE->material pdf name: '{}'", pdfName);
                    handleQueue(destination, pdfName, materialUrl);
                }
            } else if (materialUrl != null) {
                final String pdfName = "advertentie/" + Optional.ofNullable(destination.isCollaborator())
                        .filter(Boolean::booleanValue)
                        .map(destinationIsCollaborator -> order.makeMaterialPdfName(material, destination))
                        .orElse(material.makeMaterialPdfName());

                LOGGER.info("material pdf name: '{}'", pdfName);
                handleQueue(destination, pdfName, materialUrl);

            }
        } catch (final Exception e) {
            throw new MaterialException(String.format("Error queuing material to %s", destination.toString()), e);
        }

        LOGGER.debug("Material: {} done with {}", id, destination);
    }

    private void handleQueue(final MaterialDestination destination, final String path, final String redirect) {
        // TODO External error
        final ByteArrayInputStream input = new ByteArrayInputStream(new byte[0]);

        final ObjectMetadata metadata = new ObjectMetadata();
        metadata.setContentLength(0);
        metadata.setHeader(Headers.REDIRECT_LOCATION, redirect);

        // TODO Sanity check. Remove when TaskType and MaterialDestination are merged
        final String queuePath = TaskType.valueOf(destination.name()).name() + "/" + path;

        s3.putObject(queueBucket, queuePath, input, metadata);
    }

    private Material updateLineup(final Material material) {
        lineupService.setMaterialStatusById(lineupService.intoId(material.getId()), STATUS_READY);

        return material;
    }

    public Optional<Material> findMaterialById(final int materialId) {
        return Optional.ofNullable(materialRepository.findOne(materialId));
    }

    public Collection<Material> updateMaterialDestinations(final Order order, final Collection<Material> materials) {
        Collection<Material> result = createOrUpdateMaterials(materials);
        return distributeMaterials(order, result);
    }

    private Collection<Material> createOrUpdateMaterials(final Collection<Material> materials) {

        materials.stream().forEach(material -> {
            Optional.ofNullable(material.getMaterialUrl())
                    .filter(url -> "".equals(url))
                    .ifPresent(url -> material.setMaterialUrl(null));

            Optional.ofNullable(material.getPreviewUrl())
                    .filter(url -> "".equals(url))
                    .ifPresent(url -> material.setPreviewUrl(null));
        });

        LOGGER.info("materials: {}", JsonbBuilder.create().toJson(materials));

        final List<Integer> materialIds = materials.stream().map(Material::getId).collect(Collectors.toList());
        final Map<Integer, Material> materialsToUpdateById = materials.stream()
                .collect(Collectors.toMap(Material::getId, Function.identity()));
        final Map<Integer, Material> alreadyStoredMaterials = materialRepository
                .findById(materialIds.toArray(new Integer[materials.size()])).stream()
                .collect(Collectors.toMap(Material::getId, Function.identity()));

        alreadyStoredMaterials.entrySet().stream().forEach(material -> {
            material.getValue().getDestinations()
                    .addAll(materialsToUpdateById.get(material.getKey()).getDestinations());
            materialsToUpdateById.remove(material.getKey());
        });

        final Collection<Material> materialsToSave = new ArrayList<>(materialsToUpdateById.values());
        materialsToSave.addAll(alreadyStoredMaterials.values());

        materialRepository.save(materialsToSave);

        return materialsToSave;
    }
}
