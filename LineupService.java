package net.persgroep.adhub.lineup;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.datacontract.schemas._2004._07.sangwebapi_v1.AdSearchResultRecordType;
import org.datacontract.schemas._2004._07.sangwebapi_v1.ArrayOfAdSearchResultRecordType;
import org.datacontract.schemas._2004._07.sangwebapi_v1.ArrayOfCustomerSearchResultRecord;
import org.datacontract.schemas._2004._07.sangwebapi_v1.ArrayOfMediaSearchResultRecordType;
import org.datacontract.schemas._2004._07.sangwebapi_v1.ArrayOfPlacementSearchResultRecordType;
import org.datacontract.schemas._2004._07.sangwebapi_v1.ArrayOfXOrderSearchResultRecordType;
import org.datacontract.schemas._2004._07.sangwebapi_v1.CustomerRecordType;
import org.datacontract.schemas._2004._07.sangwebapi_v1.CustomerSearchResultRecord;
import org.datacontract.schemas._2004._07.sangwebapi_v1.MediaSearchResultRecordType;
import org.datacontract.schemas._2004._07.sangwebapi_v1.PlacementSearchResultRecordType;
import org.datacontract.schemas._2004._07.sangwebapi_v1.XOrderSearchResultRecordType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ad_point.ArrayOfAD;
import com.ad_point.ArrayOfInt;
import com.ad_point.ArrayOfMaterial;
import com.ad_point.EventStatusCode;
import com.ad_point.ExtReferenceTypeCode;
import com.ad_point.ExternalReferenceRecord;
import com.ad_point.GetExternalReferenceByObjectIDs;
import com.ad_point.GetExternalReferenceByObjectIDsResponse;
import com.ad_point.Material;
import com.ad_point.PlaceAds;
import com.ad_point.PlaceAdsResponse;
import com.ad_point.RegisterExternalReferenceSource;
import com.ad_point.RegisterExternalReferenceSourceResponse;
import com.ad_point.SetExternalReference;
import com.ad_point.SetExternalReferenceResponse;
import com.ad_point.UpdateEventStatuses;
import com.ad_point.UpdateEventStatusesResponse;
import com.ad_point.UpdateMaterials;
import com.ad_point.UpdateMaterialsResponse;

import net.persgroep.adhub.core.CoreUtils;
import net.persgroep.adhub.exception.AdHubException;
import net.persgroep.adhub.infrastructure.aws.SystemPropertyResolver;

public class LineupService {
	public static final String GET_REFERENCES_METHOD = "http://www.ad-point.com/GetExternalReferenceByObjectIDs";

	private static final String SET_REFERENCE_METHOD = "http://www.ad-point.com/SetExternalReference";

	public static final String ADS_BY_ORDER_ID_METHOD = "ads?OrderID=%s&MaxRecords=1000000";

	public static final String ADS_BY_CUSTOMER_ID_AND_PUBLICATION_PERIOD_ID_METHOD = "ads?CustomerID=%s&PublDateFrom=%s@PublDateTo=%s&MaxRecords=1000000";

	public static final String CUSTOMER_BY_ID_METHOD = "Customer/%s";

	private static final String ORDERS_METHOD = "DPNorders?EventSubscriber=DPN_AV&MaxRecords=%s";

	private static final String ORDERS_BY_ID_METHOD = ORDERS_METHOD + "&OrderID=%s";

	public static final String ORDERS_BY_STATUS_METHOD = ORDERS_METHOD + "&EventStatusCode=%s";

	public static final String MEDIA_BY_ID_METHOD = "Media?MediaID=%s";

	public static final String MEDIA = "Media";

	private static final String EVENTS_STATUS_METHOD = "http://www.ad-point.com/UpdateEventStatuses";

	private static final String MATERIAL_STATUS_METHOD = "http://www.ad-point.com/UpdateMaterials";

	private static final String PLACE_ADS_STATUS_METHOD = "http://www.ad-point.com/PlaceAds";

	private static final String NEW_REFERENCE_SOURCE_METHOD = "http://www.ad-point.com/RegisterExternalReferenceSource";

	public static final String PLACEMENT_METHOD = "Placement";

	private static final String MAX_ORDER_RECORDS = "MAX_ORDER_RECORDS";

	private static final String CUSTOMER_BY_OWNER_URL = "Customers?CustomerOwner=%s&MaxRecords=1000000";

	private final int nuid;

	private final int idOffset;

	private int maxOrderRecords = 150;

	private SystemPropertyResolver propertyResolver = new SystemPropertyResolver();

	private LineupResource lineupResource;

	private static final Logger LOGGER = LoggerFactory.getLogger(LineupService.class);

	protected LineupService(final LineupServiceConfig config, final LineupResource lineupResource) {

		nuid = config.getNuid();
		idOffset = config.getIdOffset();
		try {
			maxOrderRecords = Integer.parseInt(propertyResolver.getEnvironmentVariable(MAX_ORDER_RECORDS));
		} catch (Exception e) {
			LOGGER.trace("maxOrderRecords niet gezet, gebruik default ({})", maxOrderRecords);
		}

		this.lineupResource = lineupResource;
	}

	public LineupService(final LineupServiceConfig config) {
		this(config, new LineupResource(config));
	}

	public int fromId(final int value) {
		if (idOffset != 0 && idOffset < value) {
			throw new AdHubException("Invalid id %s", value);
		}

		return value + idOffset;
	}

	public int intoId(int value) {
		if ((idOffset != 0) && (idOffset > value)) {
			throw new AdHubException("Invalid id %s", value);
		}

		return value - idOffset;
	}

	private static ArrayOfInt mapArrayOfInt(Collection<Integer> src) {
		ArrayOfInt dst = new ArrayOfInt();
		dst.getInt().addAll(src);

		return dst;
	}

	public List<AdSearchResultRecordType> getAdsByOrderId(int orderId) {
		return lineupResource.getRest(ArrayOfAdSearchResultRecordType.class, ADS_BY_ORDER_ID_METHOD, orderId)
				.getAdSearchResultRecord();
	}

	public List<AdSearchResultRecordType> getAdsByCustomerIdAndPublicationPeriod(int customerId,
			Date startPublicationDate, Date endPublicationDate) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
		return lineupResource.getRest(ArrayOfAdSearchResultRecordType.class,
				ADS_BY_CUSTOMER_ID_AND_PUBLICATION_PERIOD_ID_METHOD, customerId,
				simpleDateFormat.format(startPublicationDate), simpleDateFormat.format(endPublicationDate))
				.getAdSearchResultRecord();
	}

	public CustomerRecordType getCustomerById(int id) {

		CustomerRecordType customerRecordType;
		customerRecordType = lineupResource.getRest(CustomerRecordType.class, CUSTOMER_BY_ID_METHOD, id);

		customerRecordType.getInvoicingAddress().setName(CoreUtils.getFirstLine(customerRecordType.getInvoicingAddress().getName()));
		customerRecordType.getInvoicingAddress().setStreet(CoreUtils.getFirstLine(customerRecordType.getInvoicingAddress().getStreet()));
		customerRecordType.getInvoicingAddress().setStreet2(CoreUtils.getFirstLine(customerRecordType.getInvoicingAddress().getStreet2()));
		customerRecordType.getInvoicingAddress().setStreet3(CoreUtils.getFirstLine(customerRecordType.getInvoicingAddress().getStreet3()));
		customerRecordType.getInvoicingAddress().setStreet4(CoreUtils.getFirstLine(customerRecordType.getInvoicingAddress().getStreet4()));
		customerRecordType.getMailAddress().setName(CoreUtils.getFirstLine(customerRecordType.getMailAddress().getName()));
		customerRecordType.getMailAddress().setStreet(CoreUtils.getFirstLine(customerRecordType.getMailAddress().getStreet()));
		customerRecordType.getMailAddress().setStreet2(CoreUtils.getFirstLine(customerRecordType.getMailAddress().getStreet2()));
		customerRecordType.getMailAddress().setStreet3(CoreUtils.getFirstLine(customerRecordType.getMailAddress().getStreet3()));
		customerRecordType.getMailAddress().setStreet4(CoreUtils.getFirstLine(customerRecordType.getMailAddress().getStreet4()));
		customerRecordType.getVisitingAddress().setName(CoreUtils.getFirstLine(customerRecordType.getVisitingAddress().getName()));
		customerRecordType.getVisitingAddress().setStreet(CoreUtils.getFirstLine(customerRecordType.getVisitingAddress().getStreet()));
		customerRecordType.getVisitingAddress().setStreet2(CoreUtils.getFirstLine(customerRecordType.getVisitingAddress().getStreet2()));
		customerRecordType.getVisitingAddress().setStreet3(CoreUtils.getFirstLine(customerRecordType.getVisitingAddress().getStreet3()));
		customerRecordType.getVisitingAddress().setStreet4(CoreUtils.getFirstLine(customerRecordType.getVisitingAddress().getStreet4()));
	return customerRecordType;

	}

	public List<XOrderSearchResultRecordType> getOrders() {
		return lineupResource.getRest(ArrayOfXOrderSearchResultRecordType.class,
				propertyResolver.getEnvironmentVariable(MAX_ORDER_RECORDS)).getXOrderSearchResultRecord();
	}

	public List<XOrderSearchResultRecordType> getOrdersById(int id) {
		return lineupResource.getRest(ArrayOfXOrderSearchResultRecordType.class, ORDERS_BY_ID_METHOD,
				propertyResolver.getEnvironmentVariable(MAX_ORDER_RECORDS), id).getXOrderSearchResultRecord();
	}

	public List<CustomerSearchResultRecord> getCustomersByOwner(String owner) {
		return lineupResource.getRest(ArrayOfCustomerSearchResultRecord.class, CUSTOMER_BY_OWNER_URL, owner)
				.getCustomerSearchResultRecord();
	}

	public List<XOrderSearchResultRecordType> getOrdersByStatusId(int statusId) {
		int max = 30;
		List<XOrderSearchResultRecordType> result;

		do {
			result = lineupResource
					.getRest(ArrayOfXOrderSearchResultRecordType.class, ORDERS_BY_STATUS_METHOD, max, statusId)
					.getXOrderSearchResultRecord();
			LOGGER.info("Found '{}' records", result.size());
			max += 10;
		} while ((max < maxOrderRecords) && result.isEmpty());

		return result;
	}

	public MediaSearchResultRecordType getMediaById(int id) {
		List<MediaSearchResultRecordType> media = lineupResource
				.getRest(ArrayOfMediaSearchResultRecordType.class, MEDIA_BY_ID_METHOD, id).getMediaSearchResultRecord();

		return media.isEmpty() ? null : media.get(0);
	}

	public List<PlacementSearchResultRecordType> getPlacements() {
		return lineupResource.getRest(ArrayOfPlacementSearchResultRecordType.class, PLACEMENT_METHOD)
				.getPlacementSearchResultRecord();
	}

	public Map<Integer, String> getReferences(ExtReferenceTypeCode type, Set<Integer> ids, String source) {
		GetExternalReferenceByObjectIDs request = new GetExternalReferenceByObjectIDs();
		request.setNUserID(nuid);
		request.setOExternalReferenceType(type);
		request.setNObjectIDs(mapArrayOfInt(ids));
		request.setSExternalReferenceSource(source);

		GetExternalReferenceByObjectIDsResponse response = lineupResource
				.getSang(GetExternalReferenceByObjectIDsResponse.class, GET_REFERENCES_METHOD, request);

		return response.getGetExternalReferenceByObjectIDsResult().getExternalReferenceRecord().stream().collect(
				Collectors.toMap(ExternalReferenceRecord::getObjectID, ExternalReferenceRecord::getStringValue));
	}

	public void newReferenceSource(String source) {
		RegisterExternalReferenceSource request = new RegisterExternalReferenceSource();
		request.setNUserID(nuid);
		request.setSExternalReferenceSource(source);

		lineupResource.getSang(RegisterExternalReferenceSourceResponse.class, NEW_REFERENCE_SOURCE_METHOD, request);
	}

	public void setReference(ExtReferenceTypeCode type, int id, String source, String value) {
		SetExternalReference request = new SetExternalReference();
		request.setNUserID(nuid);
		request.setNObjectID(id);
		request.setOExternalReferenceType(type);
		request.setSExternalReferenceSource(source);
		request.setSStringValue(value);

		lineupResource.getSang(SetExternalReferenceResponse.class, SET_REFERENCE_METHOD, request);
	}

	public void setEventsStatusById(Set<Integer> ids, EventStatusCode status) {
		UpdateEventStatuses request = new UpdateEventStatuses();

		try {
			XMLGregorianCalendar beginVorigeEeuw = DatatypeFactory.newInstance().newXMLGregorianCalendar(1900, 1, 1, 1,
					1, 0, 0, 0);
			request.setRemoveEventAfter(beginVorigeEeuw);
		} catch (DatatypeConfigurationException e) {
			LOGGER.error("", e);
			throw new AdHubException(e);
		}

		request.setEventIDs(mapArrayOfInt(ids));
		request.setEventStatus(status);
		request.setNUID(nuid);

		lineupResource.getSang(UpdateEventStatusesResponse.class, EVENTS_STATUS_METHOD, request);
	}

	public void newMaterialEvent(int id) {
		Material material = new Material();
		material.setMaterialID(id);

		ArrayOfMaterial materials = new ArrayOfMaterial();
		materials.setMaterial(Collections.singletonList(material));

		UpdateMaterials request = new UpdateMaterials();
		request.setNUID(nuid);
		request.setMaterials(materials);

		lineupResource.getSangPpi(UpdateMaterialsResponse.class, MATERIAL_STATUS_METHOD, request);
	}

	public void setMaterialStatusById(int id, int statusId) {
		Material material = new Material();
		material.setMaterialID(id);
		material.setNMaterialStatusID(statusId);

		ArrayOfMaterial materials = new ArrayOfMaterial();
		materials.setMaterial(Collections.singletonList(material));

		UpdateMaterials request = new UpdateMaterials();
		request.setNUID(nuid);
		request.setMaterials(materials);

		lineupResource.getSangPpi(UpdateMaterialsResponse.class, MATERIAL_STATUS_METHOD, request);
	}

	public String placeAds() {
		final PlaceAds placeAds = new PlaceAds();

		final ArrayOfAD arrayOfAD = new ArrayOfAD();

		placeAds.setAds(arrayOfAD);
		placeAds.setNUID(nuid);

		final PlaceAdsResponse placeAdsResponse = lineupResource.getSangPpi(PlaceAdsResponse.class, PLACE_ADS_STATUS_METHOD,
				placeAds);

		return placeAdsResponse.getPlaceAdsResult();
	}

	public List<MediaSearchResultRecordType> getMedia() {
		return lineupResource.getRest(ArrayOfMediaSearchResultRecordType.class, MEDIA).getMediaSearchResultRecord();
	}
}
