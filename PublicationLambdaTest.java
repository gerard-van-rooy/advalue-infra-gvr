package net.persgroep.adhub.publication;

import com.microsoft.sqlserver.jdbc.SQLServerDataSource;
import net.persgroep.adhub.blob.BlobStore;
import net.persgroep.adhub.blob.S3BlobStore;
import net.persgroep.adhub.core.MaterialDestination;
import net.persgroep.adhub.publication.partner.PartnerPublication;
import org.junit.Test;
import org.mockito.Mock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import static java.lang.Integer.parseInt;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class PublicationLambdaTest {
    @Mock
    private PublicationContext context;

    final Logger LOGGER = LoggerFactory.getLogger(PublicationLambdaTest.class);

    @Test
    public void collaboratorsTest() {

        System.setProperty("Profile", "development");
        SQLServerDataSource src = new SQLServerDataSource();
        src.setURL("jdbc:sqlserver://logship-dev.ct77j2mrb3ce.eu-west-1.rds.amazonaws.com:2433;database=logshippingetl;user=logshipuser;password=10130P3SDDPL;encrypt=true;trustServerCertificate=true;");

        int srcIdOffset = 0;

        String dstBucket = "adhub-test";


        PublicationDispatcher pd = new PublicationDispatcher(new PublicationContext() {

            @Override
            public BlobStore getDst(String prefix) {
                return new S3BlobStore(dstBucket, prefix);
            }

            @Override
            public DataSource getSrc() {
                return src;
            }

            @Override
            public int getSrcIdOffset() {
                return srcIdOffset;
            }
        });

        pd.dispatch();

        int t = 0;

        LOGGER.info("size totaal: " + pd.partnerPublications.size());
        t += filterPartner(pd.partnerPublications, MaterialDestination.AMSTERDAM_MEDIA.getMediaBrand()).size();
        t += filterPartner(pd.partnerPublications, MaterialDestination.BORNSE_COURANT.getMediaBrand()).size();
        t += filterPartner(pd.partnerPublications, MaterialDestination.BOSSCHE_OMROEP.getMediaBrand()).size();
        t += filterPartner(pd.partnerPublications, MaterialDestination.BRUGMEDIA.getMediaBrand()).size();
        t += filterPartner(pd.partnerPublications, MaterialDestination.BUIJZE_PERS.getMediaBrand()).size();
        t += filterPartner(pd.partnerPublications, MaterialDestination.GRAFICELLY.getMediaBrand()).size();
        t += filterPartner(pd.partnerPublications, MaterialDestination.KONTAKT_MEDIAPARTNERS.getMediaBrand()).size();
        t += filterPartner(pd.partnerPublications, MaterialDestination.MEDIA_TOTAAL_NOORD.getMediaBrand()).size();
        t += filterPartner(pd.partnerPublications, MaterialDestination.NDC_MEDIAGROEP.getMediaBrand()).size();
        t += filterPartner(pd.partnerPublications, MaterialDestination.OK_KRANTEN.getMediaBrand()).size();
        t += filterPartner(pd.partnerPublications, MaterialDestination.REGIOBODE.getMediaBrand()).size();
        t += filterPartner(pd.partnerPublications, MaterialDestination.RODI_MEDIA.getMediaBrand()).size();
        t += filterPartner(pd.partnerPublications, MaterialDestination.ROND_HAAKSBERGEN.getMediaBrand()).size();
        t += filterPartner(pd.partnerPublications, MaterialDestination.STARTERS_PERS.getMediaBrand()).size();
        t += filterPartner(pd.partnerPublications, MaterialDestination.STICHTING_BRABANTS_CENTRUM.getMediaBrand()).size();
        t += filterPartner(pd.partnerPublications, MaterialDestination.STUDIO_PANNEKOEK.getMediaBrand()).size();
        t += filterPartner(pd.partnerPublications, MaterialDestination.TALVI_MEDIA.getMediaBrand()).size();

        LOGGER.info("size subtotalen: " +  t);

        LOGGER.info("size AMSTERDAM_MEDIA: " + filterPartner(pd.partnerPublications, MaterialDestination.AMSTERDAM_MEDIA.getMediaBrand()).size());
        LOGGER.info("size BORNSE_COURANT: " + filterPartner(pd.partnerPublications, MaterialDestination.BORNSE_COURANT.getMediaBrand()).size());
        LOGGER.info("size BOSSCHE_OMROEP: " + filterPartner(pd.partnerPublications, MaterialDestination.BOSSCHE_OMROEP.getMediaBrand()).size());
        LOGGER.info("size BRUGMEDIA: " + filterPartner(pd.partnerPublications, MaterialDestination.BRUGMEDIA.getMediaBrand()).size());
        LOGGER.info("size BUIJZE_PERS: " + filterPartner(pd.partnerPublications, MaterialDestination.BUIJZE_PERS.getMediaBrand()).size());
        LOGGER.info("size GRAFICELLY: " + filterPartner(pd.partnerPublications, MaterialDestination.GRAFICELLY.getMediaBrand()).size());
        LOGGER.info("size KONTAKT_MEDIAPARTNERS: " + filterPartner(pd.partnerPublications, MaterialDestination.KONTAKT_MEDIAPARTNERS.getMediaBrand()).size());
        LOGGER.info("size MEDIA_TOTAAL_NOORD: " + filterPartner(pd.partnerPublications, MaterialDestination.MEDIA_TOTAAL_NOORD.getMediaBrand()).size());
        LOGGER.info("size NDC_MEDIAGROEP: " + filterPartner(pd.partnerPublications, MaterialDestination.NDC_MEDIAGROEP.getMediaBrand()).size());
        LOGGER.info("size OK_KRANTEN: " + filterPartner(pd.partnerPublications, MaterialDestination.OK_KRANTEN.getMediaBrand()).size());
        LOGGER.info("size REGIOBODE: " + filterPartner(pd.partnerPublications, MaterialDestination.REGIOBODE.getMediaBrand()).size());
        LOGGER.info("size RODI_MEDIA: " + filterPartner(pd.partnerPublications, MaterialDestination.RODI_MEDIA.getMediaBrand()).size());
        LOGGER.info("size ROND_HAAKSBERGEN: " + filterPartner(pd.partnerPublications, MaterialDestination.ROND_HAAKSBERGEN.getMediaBrand()).size());
        LOGGER.info("size STARTERS_PERS: " + filterPartner(pd.partnerPublications, MaterialDestination.STARTERS_PERS.getMediaBrand()).size());
        LOGGER.info("size STICHTING_BRABANTS_CENTRUM: " + filterPartner(pd.partnerPublications, MaterialDestination.STICHTING_BRABANTS_CENTRUM.getMediaBrand()).size());
        LOGGER.info("size STUDIO_PANNEKOEK: " + filterPartner(pd.partnerPublications, MaterialDestination.STUDIO_PANNEKOEK.getMediaBrand()).size());
        LOGGER.info("size TALVI_MEDIA: " + filterPartner(pd.partnerPublications, MaterialDestination.TALVI_MEDIA.getMediaBrand()).size());


        assertEquals(pd.partnerPublications.size(), t);
    }

    private List<PartnerPublication> filterPartner(List<PartnerPublication> partnerPublications, String mediaBrandName) {
        return partnerPublications.stream()
                .filter(partnerPublication -> partnerPublication.getPublisherName().equalsIgnoreCase(mediaBrandName))
                .collect(Collectors.toList());
    }
}
